{
    "id": "db41ef86-271a-495f-91b1-d510019cfd01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_item_screen",
    "eventList": [
        {
            "id": "2554c610-55e0-4687-ae41-68247bb97db5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db41ef86-271a-495f-91b1-d510019cfd01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "374abdee-aa48-4995-a5da-70beca837e7e",
    "visible": true
}