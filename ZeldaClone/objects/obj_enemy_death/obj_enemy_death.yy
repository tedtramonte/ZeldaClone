{
    "id": "07cd8592-683f-478c-bb13-f504a930a868",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_death",
    "eventList": [
        {
            "id": "53bdffa0-b9f2-4a6b-82ff-926ca353a971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07cd8592-683f-478c-bb13-f504a930a868"
        },
        {
            "id": "d1aeb142-3ae4-4347-b7b4-4c92e3e138a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "07cd8592-683f-478c-bb13-f504a930a868"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
    "visible": true
}