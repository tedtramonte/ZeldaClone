//as long as the player exists
if (instance_exists(obj_player)){
	//location to draw top row of hearts
	xt = 948;
	yt = 215;
	
	for (var i=0;i<obj_player.hp_containers;i++){
		ht_index=2;
		if(i>7){
			//location to draw bottom row of hearts
			xt=884;
			yt=206;
		}
		if(obj_player.hp >= (i+1)*8){
			ht_index=0;
		}
		else if(obj_player.hp >= (i+.5)*8){
			ht_index=1;
		}
		
		draw_sprite_ext(spr_heart,ht_index,xt+i*8,yt,1,1,0,c_white,1);
	}
}