{
    "id": "89900088-7988-424d-89f9-6d413e51f9d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "32100e72-0ab9-48bb-818d-b9977b59268e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "d41601b7-6def-455a-8b64-25a7b76200e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "7560789a-a8fb-4914-b92a-599de501367d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "312699e1-f673-4e26-9ea7-8534d4606c8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "b45a047f-c033-4c0e-9c4b-1c6a20498c63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b39a308d-d8b4-4a8d-ba1c-1e4d51ed7120",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "30829c03-309f-47df-baf9-fbbc1442b075",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "3306b7b0-ac9c-4da7-98d7-f0b151175063",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "42863a8b-44e0-40d5-ba55-efc869345f2f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        },
        {
            "id": "f334048e-5440-48b9-9ece-d1eff8c3096b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cc7c95c1-ad37-4707-b085-f784b1956b41",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89900088-7988-424d-89f9-6d413e51f9d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4a848f99-1cc0-44dc-9d1a-483c31a965e8",
    "visible": true
}