vx = 0;
vy = 0;
player_speed = 1;
timer = 0;
state = "IDLE";
facing = 2; // 0 up, 1 right, 2 down, 3 left

iframes=0;

hp=24;
hp_containers=3;
hp_max_containers=16;

sprite_walk = spr_player_down;
sprite_attack = spr_player_attack_down;