{
    "id": "7145df48-33f5-49e9-ba2e-db9acbeb3e38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_top",
    "eventList": [
        {
            "id": "b5a59c12-8f9d-44f8-ae50-f9f30a4387f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7145df48-33f5-49e9-ba2e-db9acbeb3e38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "2ea39ef8-d925-46d2-940f-d2ca9ceb4839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8d5108cd-11eb-4821-8748-4d739ec25c48",
    "visible": false
}