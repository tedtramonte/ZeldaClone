{
    "id": "6830d00c-7d21-4fbf-b315-3322a89cadb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sword",
    "eventList": [
        {
            "id": "1a42a85b-b867-4bba-bb43-b4e70d8db692",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6830d00c-7d21-4fbf-b315-3322a89cadb5"
        },
        {
            "id": "bdc83da5-4837-4b4a-ad78-73976d61a2ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6830d00c-7d21-4fbf-b315-3322a89cadb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3194d6b2-4b99-411c-a0d3-ec8880d9b316",
    "visible": true
}