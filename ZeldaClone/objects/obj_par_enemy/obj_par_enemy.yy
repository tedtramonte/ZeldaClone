{
    "id": "cc7c95c1-ad37-4707-b085-f784b1956b41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_par_enemy",
    "eventList": [
        {
            "id": "0049aef0-2a6b-4503-b229-69b93a3b1a5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc7c95c1-ad37-4707-b085-f784b1956b41"
        },
        {
            "id": "c0ab7cfb-5b61-43da-9a85-4df5a6e1970c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6830d00c-7d21-4fbf-b315-3322a89cadb5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cc7c95c1-ad37-4707-b085-f784b1956b41"
        },
        {
            "id": "eb792075-adf7-40ab-90dd-6570d4647a6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cc7c95c1-ad37-4707-b085-f784b1956b41"
        },
        {
            "id": "6e297db8-54ba-46dd-8923-dbefbfcffdf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cc7c95c1-ad37-4707-b085-f784b1956b41"
        },
        {
            "id": "266268f5-93db-43b3-b55c-29fa1b0a8237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cc7c95c1-ad37-4707-b085-f784b1956b41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}