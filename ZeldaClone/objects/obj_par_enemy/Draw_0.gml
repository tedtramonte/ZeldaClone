//This will make the enemy flash while it has iframes.
//Every 3rd frame, it'll skip drawing itself and disappear
if ((iframes<0) || (iframes % 3 == 0)) draw_self();

if (global.debug){
	draw_set_color(c_red);
	draw_rectangle(bbox_left,bbox_top,bbox_right,bbox_bottom,true);
}