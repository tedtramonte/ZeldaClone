{
    "id": "312699e1-f673-4e26-9ea7-8534d4606c8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heart",
    "eventList": [
        {
            "id": "c5891265-2d15-441f-b187-cafde54d70be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "312699e1-f673-4e26-9ea7-8534d4606c8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
    "visible": true
}