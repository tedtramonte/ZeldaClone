{
    "id": "42863a8b-44e0-40d5-ba55-efc869345f2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_room_warp",
    "eventList": [
        {
            "id": "0ee23318-a1d8-4ec8-9298-4cda96c09400",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42863a8b-44e0-40d5-ba55-efc869345f2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ea397c55-6ce6-47a9-a5c6-943bd2b4492b",
    "visible": true
}