{
    "id": "16c98587-053d-4b71-93fd-1c1a7a422bb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "cf9cbc8a-d46d-4745-9182-97eb251ffa23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16c98587-053d-4b71-93fd-1c1a7a422bb7"
        },
        {
            "id": "6f8cbc1b-e142-4998-8e6a-dd20513153b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16c98587-053d-4b71-93fd-1c1a7a422bb7"
        },
        {
            "id": "e6f62063-4ea5-4629-85e2-7890b01a9afe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 10,
            "m_owner": "16c98587-053d-4b71-93fd-1c1a7a422bb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}