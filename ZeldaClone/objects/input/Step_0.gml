left = keyboard_check(vk_left);
right = keyboard_check(vk_right);
up = keyboard_check(vk_up);
down = keyboard_check(vk_down);
button_a = keyboard_check_pressed(vk_space);
button_b = keyboard_check_pressed(vk_enter);
start = keyboard_check_pressed(vk_control);
select = keyboard_check_pressed(vk_alt);

if (global.gamepad) {
	left = left || gamepad_button_check(0,gp_padl);
	right = right || gamepad_button_check(0,gp_padr);
	up = up || gamepad_button_check(0,gp_padu);
	down = down || gamepad_button_check(0,gp_padd);
	select = select || gamepad_button_check_pressed(0,gp_select);
	start = start || gamepad_button_check_pressed(0,gp_start);
	button_b = button_b || gamepad_button_check_pressed(0, gp_face1);
	button_a = button_a || gamepad_button_check_pressed(0, gp_face2);
}