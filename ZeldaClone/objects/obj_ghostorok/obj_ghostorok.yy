{
    "id": "42e767ac-8889-4f61-bca5-3f31ca20a16b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ghostorok",
    "eventList": [
        {
            "id": "91c33f7a-f394-44e6-903d-c63c64ccff57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42e767ac-8889-4f61-bca5-3f31ca20a16b"
        },
        {
            "id": "d8624c1b-6e5d-4eb4-9dbc-139782aa48af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42e767ac-8889-4f61-bca5-3f31ca20a16b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "cc7c95c1-ad37-4707-b085-f784b1956b41",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8eefdbc2-476f-49a9-8864-386f1e0af124",
    "visible": true
}