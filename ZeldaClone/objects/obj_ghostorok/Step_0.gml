event_inherited();

switch(state){
	case "MOVING":{
		switch(facing){
			case enum_facing.up:{
				vx=0;
				vy=-move_speed;
				break;
			}
			case enum_facing.right:{
				vx=move_speed;
				vy=0;
				break;
			}
			case enum_facing.down:{
				vx=0;
				vy=move_speed;
				break;
			}
			case enum_facing.left:{
				vx=-move_speed;
				vy=0;
				break;
			}
		}
		if(irandom(100)<3) facing = choose(enum_facing.up,enum_facing.down,enum_facing.left,enum_facing.right);
		if(irandom(300)<1){
			state="SHOOTING";
			timer=0;
		}
		image_angle = facing-90;
		break;
	}
	case "SHOOTING":{
		vx=0;
		vy=0;
		if (timer<90){
			if(timer==60){
				with(instance_create_depth(x,y,0,obj_iceball)){
					speed = 4;
					image_angle = other.image_angle;
					direction = other.image_angle+90;
				}
			}
			timer++;
		}
		else{
			state = "MOVING";
		}
		break;
	}
}

if(check_collision_obj(vx,vy,obj_solid)){
	facing = choose(enum_facing.up,enum_facing.down,enum_facing.left,enum_facing.right);
}