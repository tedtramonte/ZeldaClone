{
    "id": "374abdee-aa48-4995-a5da-70beca837e7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 980,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "56a32ee3-5973-438d-8949-f96510fd9039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374abdee-aa48-4995-a5da-70beca837e7e",
            "compositeImage": {
                "id": "5638a2b7-1fa8-4295-8c35-781a756b26f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56a32ee3-5973-438d-8949-f96510fd9039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefe13da-2971-495c-90ac-206b70bc617a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56a32ee3-5973-438d-8949-f96510fd9039",
                    "LayerId": "69be343a-e377-46d3-8e87-a86b07d067d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 981,
    "layers": [
        {
            "id": "69be343a-e377-46d3-8e87-a86b07d067d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "374abdee-aa48-4995-a5da-70beca837e7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}