{
    "id": "ca067e60-db36-4713-a25f-d25b75fe5ef4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_iceball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "48be417d-0ea1-4036-8401-5bf7d40606db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca067e60-db36-4713-a25f-d25b75fe5ef4",
            "compositeImage": {
                "id": "24c9654f-4760-4d52-9a53-70ea8b240cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48be417d-0ea1-4036-8401-5bf7d40606db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d5f2e1-c1d3-4e11-8ec7-0936300aac65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48be417d-0ea1-4036-8401-5bf7d40606db",
                    "LayerId": "2162ba3f-3035-4040-a696-99cf77418b06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2162ba3f-3035-4040-a696-99cf77418b06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca067e60-db36-4713-a25f-d25b75fe5ef4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}