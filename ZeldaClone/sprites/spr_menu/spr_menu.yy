{
    "id": "5ba08f2d-1588-4a0e-8df4-b297e5befe02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4cd0a465-ef79-4db8-bc63-5dd9a914ad60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ba08f2d-1588-4a0e-8df4-b297e5befe02",
            "compositeImage": {
                "id": "73066a9f-fd72-4dc6-b9a8-e4b873aaffbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd0a465-ef79-4db8-bc63-5dd9a914ad60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aeda19f-7658-413b-84a6-3f574d99401e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd0a465-ef79-4db8-bc63-5dd9a914ad60",
                    "LayerId": "dbd5f3c4-8d56-4b35-a70e-88031cb9a26e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "dbd5f3c4-8d56-4b35-a70e-88031cb9a26e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ba08f2d-1588-4a0e-8df4-b297e5befe02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}