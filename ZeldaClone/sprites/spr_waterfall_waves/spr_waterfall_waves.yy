{
    "id": "0efeb3ae-146a-48fc-90e2-98a17494b638",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_waterfall_waves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b015f235-4ad5-4485-99c5-a12f92143f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "14a4a2d9-0644-4581-b4ce-ae385ddc0a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b015f235-4ad5-4485-99c5-a12f92143f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0e054a-ec8d-47b1-a967-ef0853831f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b015f235-4ad5-4485-99c5-a12f92143f53",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "aec0ee05-7a50-45f3-87e2-80d3c979a0ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "cfa7c91e-0587-46c3-a9e3-55156c9fa9ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec0ee05-7a50-45f3-87e2-80d3c979a0ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30cc4279-3a70-40a5-9d16-b47c4b435502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec0ee05-7a50-45f3-87e2-80d3c979a0ae",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "db747efe-3d89-42d8-9e9b-7677b9ec365a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "802e5ac4-b05d-4769-9a93-59c03fc5bf2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db747efe-3d89-42d8-9e9b-7677b9ec365a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdb18f92-3945-44d0-bcf0-5fa62b012284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db747efe-3d89-42d8-9e9b-7677b9ec365a",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "adcd3b10-9e66-41ea-a8df-6e48b01fe895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "569ee37d-775a-4d16-b244-7b91476da44e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcd3b10-9e66-41ea-a8df-6e48b01fe895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a664587-e654-4efc-b48e-4be5b3420942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcd3b10-9e66-41ea-a8df-6e48b01fe895",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "8fcbfbed-45b0-4ac5-89de-cc5dc659fb6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "4013f9a5-0e42-4494-9fc1-4d9df1a59b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fcbfbed-45b0-4ac5-89de-cc5dc659fb6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6803ae19-6df9-4c78-b4dd-de25a8f2c161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fcbfbed-45b0-4ac5-89de-cc5dc659fb6d",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "8511ff5d-f613-4337-946a-cb4f1f5683fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "42ea8a74-f852-4356-be07-24804aa0d307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8511ff5d-f613-4337-946a-cb4f1f5683fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dcec894-67fa-4d08-b04d-937d3f7890c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8511ff5d-f613-4337-946a-cb4f1f5683fb",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "01bc5dad-b0cc-41a9-acd4-2213cb474a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "43109fa8-a664-4775-9484-e7bb91ba420c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bc5dad-b0cc-41a9-acd4-2213cb474a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af84a92f-3c3e-4c20-9dcc-00d3c6d682f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bc5dad-b0cc-41a9-acd4-2213cb474a51",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "35763c86-2016-4878-a4a3-fd20851d9145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "1bcaf260-5fd4-4945-88b6-23af8ef22c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35763c86-2016-4878-a4a3-fd20851d9145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0bc85a6-a3a9-4f48-9023-dec7aad5f103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35763c86-2016-4878-a4a3-fd20851d9145",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        },
        {
            "id": "9319faba-8d5d-4045-ae3d-9acf83d71df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "compositeImage": {
                "id": "b09d674d-afb8-4bc1-bc12-6e78c339ee95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9319faba-8d5d-4045-ae3d-9acf83d71df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "372ed3ab-abf2-468e-ab01-bec2991925f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9319faba-8d5d-4045-ae3d-9acf83d71df2",
                    "LayerId": "f7c15f10-0501-4ea4-b253-42209a8961a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f7c15f10-0501-4ea4-b253-42209a8961a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0efeb3ae-146a-48fc-90e2-98a17494b638",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}