{
    "id": "8eefdbc2-476f-49a9-8864-386f1e0af124",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ghostorok",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "84c1dcbb-99fc-43fe-9624-801c87ce99eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eefdbc2-476f-49a9-8864-386f1e0af124",
            "compositeImage": {
                "id": "856c8688-f2e5-4b78-938e-1661b108803f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c1dcbb-99fc-43fe-9624-801c87ce99eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c8ef54-1b44-4309-978b-3eb08aadd5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c1dcbb-99fc-43fe-9624-801c87ce99eb",
                    "LayerId": "eefe1f44-6be7-43f7-ab4b-8b1a38ab9643"
                }
            ]
        },
        {
            "id": "249f17ba-1199-478d-ba60-016bb4da8fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eefdbc2-476f-49a9-8864-386f1e0af124",
            "compositeImage": {
                "id": "510212d2-6da4-4ee6-bcb0-b251e6c4ec13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249f17ba-1199-478d-ba60-016bb4da8fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7b75bb-7d29-4630-bdee-24dc0152c8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249f17ba-1199-478d-ba60-016bb4da8fd9",
                    "LayerId": "eefe1f44-6be7-43f7-ab4b-8b1a38ab9643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "eefe1f44-6be7-43f7-ab4b-8b1a38ab9643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eefdbc2-476f-49a9-8864-386f1e0af124",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}