{
    "id": "2a0b3855-7954-4a80-8dee-be9acba14828",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d3b08c66-441b-45d8-9c36-a3cf9cbfb7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "d4ab51c6-8b8d-4f1f-955c-456df55b12bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b08c66-441b-45d8-9c36-a3cf9cbfb7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1984c2e8-c961-45f2-9ebb-6c17286a02bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b08c66-441b-45d8-9c36-a3cf9cbfb7c6",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "c5682362-ab6a-4fea-832e-f2819466df9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "a527cddf-47e0-4651-8124-5af3f24204c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5682362-ab6a-4fea-832e-f2819466df9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10838e3-1b02-49ab-b303-7c86a78e109e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5682362-ab6a-4fea-832e-f2819466df9e",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "4d43451d-f5f9-4cce-8a8b-dd9a243c0aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "e1bf0732-ff5d-4356-b9bd-b19219beaa87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d43451d-f5f9-4cce-8a8b-dd9a243c0aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f4d20e-d171-47bf-a504-645cbd8b2cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d43451d-f5f9-4cce-8a8b-dd9a243c0aba",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "be8d57d3-9961-46ea-8637-aba899f90fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "2b100869-c9c5-4311-8cc0-1b82abab88e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8d57d3-9961-46ea-8637-aba899f90fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3861ec-1162-4a67-b345-cff8094b290a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8d57d3-9961-46ea-8637-aba899f90fd6",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "6eb3fdf7-c8d6-463f-be8e-c038cef03a29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "81675e4e-ef9c-4768-8702-2c0a9d9033d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb3fdf7-c8d6-463f-be8e-c038cef03a29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b61b75-2515-4995-b84f-9c37c1f05cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb3fdf7-c8d6-463f-be8e-c038cef03a29",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "4902d98e-5c00-4b83-b03d-eff3c33caa50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "25f78d91-7346-4582-a3f8-3f3c85e00ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4902d98e-5c00-4b83-b03d-eff3c33caa50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80af74bb-f04d-47eb-ba29-8e0369713671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4902d98e-5c00-4b83-b03d-eff3c33caa50",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "51bdfd07-cd56-4176-8771-e8e3b632d588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "07be01bb-bf67-4b8c-a1dd-f7df342d76bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51bdfd07-cd56-4176-8771-e8e3b632d588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b867d498-bf63-4dfe-a27d-484db1d408ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51bdfd07-cd56-4176-8771-e8e3b632d588",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "8dc9928b-d5b1-4c2c-a739-5f4eba98e7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "bcee7293-fff8-475e-a382-e216126fabb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc9928b-d5b1-4c2c-a739-5f4eba98e7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbac29d-84ff-4fef-87d7-7a3aee8b9d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc9928b-d5b1-4c2c-a739-5f4eba98e7fd",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "baf6343e-1269-427f-b18b-420bb0faa550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "d5d3ec29-e04d-4a53-a1ef-0e7bf5c395fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baf6343e-1269-427f-b18b-420bb0faa550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d46b84a-4c6a-4a32-8502-07bf0a0e1e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baf6343e-1269-427f-b18b-420bb0faa550",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "fd9bd475-259d-42ed-acc7-223a917e7f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "756e1125-b5f4-4e96-af48-cfcc4e8d235a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9bd475-259d-42ed-acc7-223a917e7f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4695fc99-893b-4e23-a58c-fca805acd6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9bd475-259d-42ed-acc7-223a917e7f7e",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "ddfb2d94-e707-44d4-bf80-beb493c001db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "87824bb5-0b6e-4ee9-b031-32e02c694e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddfb2d94-e707-44d4-bf80-beb493c001db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94f1c512-0980-4645-b15a-481c786853bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddfb2d94-e707-44d4-bf80-beb493c001db",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        },
        {
            "id": "6b262d36-44dc-4905-aff5-f5e3b7ff17ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "compositeImage": {
                "id": "40b06445-bb1b-4919-b46e-04cc57968a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b262d36-44dc-4905-aff5-f5e3b7ff17ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a150869c-8e4f-48c4-97a6-c918d1fda508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b262d36-44dc-4905-aff5-f5e3b7ff17ab",
                    "LayerId": "5a851674-e33b-41d7-8bd0-9e14c365ee65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5a851674-e33b-41d7-8bd0-9e14c365ee65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a0b3855-7954-4a80-8dee-be9acba14828",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}