{
    "id": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "24cb0afe-bbb2-4ba8-b408-d2146d8b862b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "07648f5e-ba1e-4b83-9832-ca3a269cf459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cb0afe-bbb2-4ba8-b408-d2146d8b862b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c1f255-315f-409f-bf8a-a223fa786d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cb0afe-bbb2-4ba8-b408-d2146d8b862b",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        },
        {
            "id": "d95fb767-8005-4888-9dcd-a78b45c262ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "3d0cc831-b586-4f44-b4cd-05075ad4e125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95fb767-8005-4888-9dcd-a78b45c262ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc5d831-f284-4354-97b6-dc5f5af9556f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95fb767-8005-4888-9dcd-a78b45c262ac",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        },
        {
            "id": "24c5696c-df95-4f7a-a78c-0e11ee7a1bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "a30b9ae4-6733-4ae3-8ef1-2d1dc5974839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c5696c-df95-4f7a-a78c-0e11ee7a1bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b6cc6c-212e-48fd-a43c-d79e8e251a01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c5696c-df95-4f7a-a78c-0e11ee7a1bda",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        },
        {
            "id": "33c514a8-a89c-4c89-9464-46f7838b0b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "e8eaea7a-b118-43c4-9e04-6cb19a6e1c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c514a8-a89c-4c89-9464-46f7838b0b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "277e6d00-f04a-4b5a-b948-687746486ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c514a8-a89c-4c89-9464-46f7838b0b4e",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        },
        {
            "id": "5e5aca30-d277-4511-abe4-9419edebdc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "88823d1a-eb05-402e-9a3e-8459a78a04ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5aca30-d277-4511-abe4-9419edebdc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668d11f0-5293-496d-bb5b-22250a858b4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5aca30-d277-4511-abe4-9419edebdc10",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        },
        {
            "id": "10dff693-61aa-45b9-89a9-ec8daa8bcc13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "compositeImage": {
                "id": "654597ee-fce7-4d72-a7a4-f4257a831b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10dff693-61aa-45b9-89a9-ec8daa8bcc13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a12d57fc-2fc6-4cf2-97c2-7c425784fb35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10dff693-61aa-45b9-89a9-ec8daa8bcc13",
                    "LayerId": "a0cc3998-b583-420b-a85c-03ef4857263c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "a0cc3998-b583-420b-a85c-03ef4857263c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42d41b9f-1d2b-4860-8df7-b1cd8eec02db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}