{
    "id": "ceab6b32-9dec-4318-9746-3684759e0d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d6d541f4-c111-4128-96d7-3c0454c910a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceab6b32-9dec-4318-9746-3684759e0d01",
            "compositeImage": {
                "id": "e0fc0876-85d3-4c8d-aea1-3c8cb1d81217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d541f4-c111-4128-96d7-3c0454c910a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d59749a-b9e1-4241-8d18-3da2b09667da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d541f4-c111-4128-96d7-3c0454c910a9",
                    "LayerId": "66159a81-521c-4683-96d2-26389be2cfad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "66159a81-521c-4683-96d2-26389be2cfad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceab6b32-9dec-4318-9746-3684759e0d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}