{
    "id": "65fb71c3-af7b-422c-aa57-30af461a4210",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a3901e41-4a01-47b6-abdb-f49f3a2eeeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "bdec3f21-b64c-4b3e-aaa5-7f6907a9a4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3901e41-4a01-47b6-abdb-f49f3a2eeeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1b9d1b-15c2-47df-9877-9014a2f4b40f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3901e41-4a01-47b6-abdb-f49f3a2eeeb9",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "a85c7010-9574-4eeb-b8c5-7784142ffe6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "6266e3d0-26dd-415c-832d-197806c70f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85c7010-9574-4eeb-b8c5-7784142ffe6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42e4177-2195-43f9-9af5-50df6d1eb25e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85c7010-9574-4eeb-b8c5-7784142ffe6d",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "383e0d72-ddb5-49d9-be9b-d59378e5f512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "dab67b44-b848-4852-b905-980555d85755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383e0d72-ddb5-49d9-be9b-d59378e5f512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9ea9428-6fee-4934-a980-24f90a1acce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383e0d72-ddb5-49d9-be9b-d59378e5f512",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "acbe6e7d-cf0b-48ef-9d10-19a4c887593a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "c539c676-ac0e-4c92-b7bb-9d25ac4870fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acbe6e7d-cf0b-48ef-9d10-19a4c887593a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "400692e1-2bb9-43da-9f35-7e1aba90c268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acbe6e7d-cf0b-48ef-9d10-19a4c887593a",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "35383f8d-e286-4ac2-8933-71306077886d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "ca1ca9c5-bef4-4254-8390-0e5214b54806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35383f8d-e286-4ac2-8933-71306077886d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7cbcdab-26f0-4876-8208-21029118b10a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35383f8d-e286-4ac2-8933-71306077886d",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "e113c6d2-e930-4ab8-9afc-c7b41aabab82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "d8e182b1-f3bb-44d3-a8b4-6e0168a4db91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e113c6d2-e930-4ab8-9afc-c7b41aabab82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd922d06-863e-4199-af39-c6266d291e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e113c6d2-e930-4ab8-9afc-c7b41aabab82",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "cc71f006-a699-4149-9a92-4132624c74a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "f32ece1a-465f-4292-b6b0-737bf8288829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc71f006-a699-4149-9a92-4132624c74a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2345b820-4610-4a59-a568-f1d1fa86bccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc71f006-a699-4149-9a92-4132624c74a7",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        },
        {
            "id": "9547778c-763d-4b5d-9dff-812a8062c747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "compositeImage": {
                "id": "1319a4f6-2415-4216-8e49-0fbc5e74e35a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9547778c-763d-4b5d-9dff-812a8062c747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d06f61b-aa17-4556-81fa-88349739b3d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9547778c-763d-4b5d-9dff-812a8062c747",
                    "LayerId": "8da2203d-b60a-42f8-ba06-5a922b085aa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8da2203d-b60a-42f8-ba06-5a922b085aa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65fb71c3-af7b-422c-aa57-30af461a4210",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}