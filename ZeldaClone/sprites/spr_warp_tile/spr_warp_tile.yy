{
    "id": "ea397c55-6ce6-47a9-a5c6-943bd2b4492b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "33b09d93-eab9-42b8-adaa-6335f34a4a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea397c55-6ce6-47a9-a5c6-943bd2b4492b",
            "compositeImage": {
                "id": "c9921489-28dd-4734-a4bc-969089db5b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b09d93-eab9-42b8-adaa-6335f34a4a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6446ccb-813b-4856-8f36-554c3fa6a4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b09d93-eab9-42b8-adaa-6335f34a4a79",
                    "LayerId": "acdaae3c-aaaa-4171-bc8d-d231cf3bc29d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "acdaae3c-aaaa-4171-bc8d-d231cf3bc29d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea397c55-6ce6-47a9-a5c6-943bd2b4492b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}