{
    "id": "ef26d467-3096-4641-a199-ac5affa8bc93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_waterfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dc300a14-5863-425a-bce3-1cbecf811149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef26d467-3096-4641-a199-ac5affa8bc93",
            "compositeImage": {
                "id": "e127a716-0d02-4155-a0fe-8b8e91a56e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc300a14-5863-425a-bce3-1cbecf811149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0388994-e8d3-4a30-ac40-9dd73b83a99d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc300a14-5863-425a-bce3-1cbecf811149",
                    "LayerId": "66ffc68c-b41b-4020-9f3b-f936b852ac40"
                }
            ]
        },
        {
            "id": "3eacbefd-2d43-46e5-9fb5-d4f254b73695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef26d467-3096-4641-a199-ac5affa8bc93",
            "compositeImage": {
                "id": "720c2ea0-7cb3-4c00-80c4-fdd87ba2987b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eacbefd-2d43-46e5-9fb5-d4f254b73695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbb4631-b15a-42fc-86df-c96577be97be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eacbefd-2d43-46e5-9fb5-d4f254b73695",
                    "LayerId": "66ffc68c-b41b-4020-9f3b-f936b852ac40"
                }
            ]
        },
        {
            "id": "52cee5bb-4f10-4220-870e-153830764928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef26d467-3096-4641-a199-ac5affa8bc93",
            "compositeImage": {
                "id": "117187fa-4f13-470f-a988-599e519fe487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52cee5bb-4f10-4220-870e-153830764928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb44b5ac-e225-45b8-ae99-c7883c508f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52cee5bb-4f10-4220-870e-153830764928",
                    "LayerId": "66ffc68c-b41b-4020-9f3b-f936b852ac40"
                }
            ]
        },
        {
            "id": "6ab525f3-0aca-4da9-b1af-bfe68cc085e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef26d467-3096-4641-a199-ac5affa8bc93",
            "compositeImage": {
                "id": "d2e78f51-aaa4-4966-95b5-3521cd769ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ab525f3-0aca-4da9-b1af-bfe68cc085e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c1ec059-4e76-47b6-9c29-c13002692430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ab525f3-0aca-4da9-b1af-bfe68cc085e7",
                    "LayerId": "66ffc68c-b41b-4020-9f3b-f936b852ac40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "66ffc68c-b41b-4020-9f3b-f936b852ac40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef26d467-3096-4641-a199-ac5affa8bc93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}