{
    "id": "9c4873d8-c752-4f87-8b3f-c3ba44158dd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6b577c90-ca38-494f-aefd-24b3f334c97f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c4873d8-c752-4f87-8b3f-c3ba44158dd8",
            "compositeImage": {
                "id": "4ea271f9-416c-410e-a3f8-761b29dcf522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b577c90-ca38-494f-aefd-24b3f334c97f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b006f703-491e-4a54-aa89-3bdfc550f63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b577c90-ca38-494f-aefd-24b3f334c97f",
                    "LayerId": "2815f0f7-5afe-4abe-bcf1-317e6687ab9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "2815f0f7-5afe-4abe-bcf1-317e6687ab9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c4873d8-c752-4f87-8b3f-c3ba44158dd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}