{
    "id": "f9e77471-3058-4887-8bc7-375b55c7e443",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heartcontainer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4a4b4852-c54c-484a-a1ae-e0c4492bddf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9e77471-3058-4887-8bc7-375b55c7e443",
            "compositeImage": {
                "id": "f7b8a9b0-dbf8-49ab-8003-8022e27d7258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4b4852-c54c-484a-a1ae-e0c4492bddf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d40573-c6cd-47b4-a805-1b27f59c8812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4b4852-c54c-484a-a1ae-e0c4492bddf3",
                    "LayerId": "fa9ec5c3-2935-48dd-88bc-8302159a777e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fa9ec5c3-2935-48dd-88bc-8302159a777e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9e77471-3058-4887-8bc7-375b55c7e443",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}