{
    "id": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b68a73d5-0529-49b9-ad07-4a76b6ad5f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
            "compositeImage": {
                "id": "95f79041-3f67-490d-a76b-1cab8b648075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68a73d5-0529-49b9-ad07-4a76b6ad5f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fe17d5-fd2a-44c4-95af-d164fe4ece05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68a73d5-0529-49b9-ad07-4a76b6ad5f5b",
                    "LayerId": "cf2258b5-4ec2-4339-a114-e04a2c065f0a"
                }
            ]
        },
        {
            "id": "3da37e1f-e8bb-42da-9dfa-24bd5a3fbd7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
            "compositeImage": {
                "id": "6e2bf9c7-8efb-4267-8a96-771af7dab1ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da37e1f-e8bb-42da-9dfa-24bd5a3fbd7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9360e421-445b-48c3-b8ba-dfaa9529e6be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da37e1f-e8bb-42da-9dfa-24bd5a3fbd7f",
                    "LayerId": "cf2258b5-4ec2-4339-a114-e04a2c065f0a"
                }
            ]
        },
        {
            "id": "4df5399b-2f65-481a-8769-c11b28ce8d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
            "compositeImage": {
                "id": "4cb0f1b0-b3b5-4038-83d0-3faf9467cb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df5399b-2f65-481a-8769-c11b28ce8d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5febae7-1d82-40f2-9485-a46096e27fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df5399b-2f65-481a-8769-c11b28ce8d87",
                    "LayerId": "cf2258b5-4ec2-4339-a114-e04a2c065f0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "cf2258b5-4ec2-4339-a114-e04a2c065f0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37d6854c-7113-4819-b3ca-c8b366f83c0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}