{
    "id": "a8bde449-b139-4a47-88c4-9a0ba00bc4a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9fba32df-9203-4f20-aee9-6a1ac12550fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8bde449-b139-4a47-88c4-9a0ba00bc4a7",
            "compositeImage": {
                "id": "80cf7f81-72b5-48ee-a0e3-8c820e8b419e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fba32df-9203-4f20-aee9-6a1ac12550fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a09c2fc-3d48-408f-b685-2114e1905846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fba32df-9203-4f20-aee9-6a1ac12550fe",
                    "LayerId": "b7717566-57ba-43f6-842f-d637e3eae688"
                }
            ]
        },
        {
            "id": "a72d0301-786e-4917-a78a-61888617eb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8bde449-b139-4a47-88c4-9a0ba00bc4a7",
            "compositeImage": {
                "id": "f0281133-f745-4a27-a902-a66e0024a464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72d0301-786e-4917-a78a-61888617eb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87822421-4a2f-4aeb-89a3-159d4afcb3cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72d0301-786e-4917-a78a-61888617eb41",
                    "LayerId": "b7717566-57ba-43f6-842f-d637e3eae688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b7717566-57ba-43f6-842f-d637e3eae688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8bde449-b139-4a47-88c4-9a0ba00bc4a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}