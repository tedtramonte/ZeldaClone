{
    "id": "ad3d9507-0187-4071-93e3-e99ffe2079f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "711d2b03-4f80-413e-8220-58ef0a3a1aef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3d9507-0187-4071-93e3-e99ffe2079f8",
            "compositeImage": {
                "id": "d1418803-64cf-4023-84ed-605df37486d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711d2b03-4f80-413e-8220-58ef0a3a1aef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af24788-a1c3-4a99-91b3-0cca258a3fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711d2b03-4f80-413e-8220-58ef0a3a1aef",
                    "LayerId": "c6c5c089-bc07-4daa-b768-97ec3a7e67e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c6c5c089-bc07-4daa-b768-97ec3a7e67e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad3d9507-0187-4071-93e3-e99ffe2079f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}