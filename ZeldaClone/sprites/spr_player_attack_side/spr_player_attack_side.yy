{
    "id": "0d7adfec-878c-479b-9f77-ad93794f2b0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7f23e1c2-d0df-4172-aca0-2be25421ae72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7adfec-878c-479b-9f77-ad93794f2b0e",
            "compositeImage": {
                "id": "ff12f9f7-7aeb-4707-804c-a49996bffee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f23e1c2-d0df-4172-aca0-2be25421ae72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a15335bb-ba6b-4152-817b-2e24b7f58913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f23e1c2-d0df-4172-aca0-2be25421ae72",
                    "LayerId": "226a75ca-9107-4b0e-a66b-f540f572b662"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "226a75ca-9107-4b0e-a66b-f540f572b662",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d7adfec-878c-479b-9f77-ad93794f2b0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}