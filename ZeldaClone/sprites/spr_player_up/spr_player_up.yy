{
    "id": "3ae50958-f6ef-4e0f-9145-496048f95a9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d4338dfe-4795-4859-a304-051ac206517c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ae50958-f6ef-4e0f-9145-496048f95a9d",
            "compositeImage": {
                "id": "45c21112-d76e-43e5-ac01-1fa770aaaee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4338dfe-4795-4859-a304-051ac206517c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "526e48a9-4466-4d44-8f35-7ba0588ec440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4338dfe-4795-4859-a304-051ac206517c",
                    "LayerId": "77899871-9528-498f-a72a-d746a3aaa8d7"
                }
            ]
        },
        {
            "id": "ccf2487d-da51-40c8-a22d-6b22bf3a1cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ae50958-f6ef-4e0f-9145-496048f95a9d",
            "compositeImage": {
                "id": "714e6dfa-37c5-42a7-806f-9a3503ea5a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf2487d-da51-40c8-a22d-6b22bf3a1cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e616802-cbc6-47a5-95be-d045be5965a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf2487d-da51-40c8-a22d-6b22bf3a1cea",
                    "LayerId": "77899871-9528-498f-a72a-d746a3aaa8d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "77899871-9528-498f-a72a-d746a3aaa8d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ae50958-f6ef-4e0f-9145-496048f95a9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}