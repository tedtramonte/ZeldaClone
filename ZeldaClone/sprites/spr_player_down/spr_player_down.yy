{
    "id": "4a848f99-1cc0-44dc-9d1a-483c31a965e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2ffad506-dfde-4dd0-84cf-cb1ce90ea379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a848f99-1cc0-44dc-9d1a-483c31a965e8",
            "compositeImage": {
                "id": "a5a432e1-f1c1-4853-bd6f-4659da40004b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ffad506-dfde-4dd0-84cf-cb1ce90ea379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da26c34a-94a3-4f52-9606-6bfc7a7659ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ffad506-dfde-4dd0-84cf-cb1ce90ea379",
                    "LayerId": "6611ff02-b83a-4e3b-8f0d-0f724be0b5d4"
                }
            ]
        },
        {
            "id": "56872c73-c660-4616-955e-7485cc8df484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a848f99-1cc0-44dc-9d1a-483c31a965e8",
            "compositeImage": {
                "id": "e2d399d4-a181-4cad-99b1-7d785b298013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56872c73-c660-4616-955e-7485cc8df484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4557a9-476c-4069-b93a-ec5cfc3e51b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56872c73-c660-4616-955e-7485cc8df484",
                    "LayerId": "6611ff02-b83a-4e3b-8f0d-0f724be0b5d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6611ff02-b83a-4e3b-8f0d-0f724be0b5d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a848f99-1cc0-44dc-9d1a-483c31a965e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}