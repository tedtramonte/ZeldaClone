{
    "id": "3194d6b2-4b99-411c-a0d3-ec8880d9b316",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a239d888-f901-4bc9-b52c-560c5f0b31dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3194d6b2-4b99-411c-a0d3-ec8880d9b316",
            "compositeImage": {
                "id": "b9ead647-8525-47d6-9d1f-c39af9bcb548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a239d888-f901-4bc9-b52c-560c5f0b31dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05253fe0-652f-44b5-b15d-14bd4310b0ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a239d888-f901-4bc9-b52c-560c5f0b31dd",
                    "LayerId": "8db9546e-15a8-4da4-b5b3-b993650e7e83"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 16,
    "layers": [
        {
            "id": "8db9546e-15a8-4da4-b5b3-b993650e7e83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3194d6b2-4b99-411c-a0d3-ec8880d9b316",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}