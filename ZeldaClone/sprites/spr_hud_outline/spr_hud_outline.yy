{
    "id": "65424f1e-5552-4247-820e-a8ce659fd314",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2de1645e-826b-44dd-ba25-063697064e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65424f1e-5552-4247-820e-a8ce659fd314",
            "compositeImage": {
                "id": "5fb11b45-d01c-4ad0-9540-7b80bdd6dc32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de1645e-826b-44dd-ba25-063697064e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec0c5cd0-7e49-4bf0-8d30-81d2c8ecb89d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de1645e-826b-44dd-ba25-063697064e2b",
                    "LayerId": "bc15cacc-8171-46cc-896c-4eab80a9b9c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "bc15cacc-8171-46cc-896c-4eab80a9b9c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65424f1e-5552-4247-820e-a8ce659fd314",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}