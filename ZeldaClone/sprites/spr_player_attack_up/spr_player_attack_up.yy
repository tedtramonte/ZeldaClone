{
    "id": "3dc6db43-bde1-42cc-9174-d573ea6dee3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7557874c-fc31-45b6-b6ec-c5e4d021a858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dc6db43-bde1-42cc-9174-d573ea6dee3b",
            "compositeImage": {
                "id": "070ba9d5-cdcc-4fc4-8b92-652e54a69ed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7557874c-fc31-45b6-b6ec-c5e4d021a858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcbec61-f1f0-4b4d-8859-49cbc128b193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7557874c-fc31-45b6-b6ec-c5e4d021a858",
                    "LayerId": "754a35da-2ba4-4d05-846d-937d80bce1d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "754a35da-2ba4-4d05-846d-937d80bce1d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dc6db43-bde1-42cc-9174-d573ea6dee3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}