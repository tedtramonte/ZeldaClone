{
    "id": "8d5108cd-11eb-4821-8748-4d739ec25c48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "310671be-845b-4ea1-859e-2a2713423e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d5108cd-11eb-4821-8748-4d739ec25c48",
            "compositeImage": {
                "id": "1291b60c-bf02-42e6-a6d7-12b90c1294f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310671be-845b-4ea1-859e-2a2713423e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d17622cd-3792-43e4-b911-c7bfb600ae5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310671be-845b-4ea1-859e-2a2713423e42",
                    "LayerId": "322dfbe6-2c75-44fd-8990-bc422cd89f38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "322dfbe6-2c75-44fd-8990-bc422cd89f38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d5108cd-11eb-4821-8748-4d739ec25c48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}