if(draw_get_alpha()<=1) draw_set_alpha(draw_get_alpha()+.02);

if(!audio_is_playing(snd_music_foreboding)) audio_play_sound(snd_music_foreboding,1,false);

if (state_time > 8 && input.start) {
	room_goto(rm_select);
	state_switch(st_game_select);
}

if (state_time > 400) {
	draw_set_alpha(draw_get_alpha()-.03);
}

if (draw_get_alpha()<=0) {
	room_goto(rm_items);
	state_switch(st_game_items);
}