m_speed=2;
hud_height=56;
level_height=176;
total_height=hud_height+level_height;
//cur_y_pos = camera_get_view_y(view_camera[1]);
if(!menu_open){
	//Opening the menu
	hud_y_target=0;
	hud_height_target=total_height;
	level_y_target=total_height;
	if (target_yview == 0){
		level_height_target=target_yview-level_height;
	}
	else {
		level_height_target=0;
	}
	
	if(view_hport[1] == total_height){
		menu_open=true;
		state_switch(st_game_in_menu);
	}
}
else {
	//closing the menu
	hud_y_target=level_height;
	hud_height_target=hud_height;
	level_y_target=hud_height;
	level_height_target= target_yview;
	
	if(view_hport[1] == hud_height){
		menu_open=false;
		state_switch(st_game_in_level);
	}
}

//HUD
camera_set_view_size(view_camera[1], 
	camera_get_view_width(view_camera[1]), 
	move_toward(camera_get_view_height(view_camera[1]), hud_height_target, m_speed));
camera_set_view_pos(view_camera[1], 
	camera_get_view_x(view_camera[1]), 
	move_toward(camera_get_view_y(view_camera[1]), hud_y_target, m_speed));
view_hport[1] = move_toward(view_hport[1], hud_height_target, m_speed);

//Level
camera_set_view_pos(view_camera[0], 
	camera_get_view_x(view_camera[0]), 
	move_toward(camera_get_view_y(view_camera[0]), level_height_target, m_speed));