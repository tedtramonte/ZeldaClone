//align_to_grid(val, alignTo);

var val = argument[0];
var alignTo = argument[1];
var diff = val mod alignTo; // gets remainder of val/alignTo
var halfway = (alignTo - 1) div 2;

if (diff > halfway) {
	return alignTo-diff;
}
else {
	return -diff;
}