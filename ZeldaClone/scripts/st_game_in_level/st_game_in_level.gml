target_xview = (obj_player.x div 256)*256;
target_yview =(obj_player.y div 176)*176;

//Snap to player when it's the first time in a room
if (state_time==0){
	camera_set_view_pos(view_camera[0], target_xview, target_yview);
}

camera_set_view_pos(view_camera[0], 
	move_toward(camera_get_view_x(view_camera[0]), target_xview, 4), 
	move_toward(camera_get_view_y(view_camera[0]), target_yview, 4));
		
if(state_time > 8 && input.start) state_switch(st_game_menu_trans);