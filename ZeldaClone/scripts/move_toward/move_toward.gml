// move_toward(start, end, val)

var _start, _end, _val;

_start = argument[0];
_end = argument[1];
_val = argument[2];

if(_start < _end){
	return min(_start+_val,_end);
}
else {
	return max(_start-_val, _end);
}