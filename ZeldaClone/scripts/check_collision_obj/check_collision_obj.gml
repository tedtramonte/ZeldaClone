var _vx, _vy, _xcollision, _ycollision;
_vx = argument[0];
_vy = argument[1];
_xcollision = false;
_ycollision = false;

for (var i=2; i<argument_count;i++){
	if(place_meeting(x+_vx,y,argument[i])){
		_xcollision = true;
	}
	if(place_meeting(x,y+_vy,argument[i])){
		_ycollision = true;
	}
}

x+=_vx;
y+=_vy;

if (_xcollision){
	x=(x div 2)*2;
	if(sign(_vx)==-1) x+=2;
}
if (_ycollision){
	y=(y div 2)*2;
	if(sign(_vy)==-1) y+=2; 
}

return _xcollision || _ycollision;