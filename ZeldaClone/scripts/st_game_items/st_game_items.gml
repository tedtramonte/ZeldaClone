draw_set_alpha(1);

if (state_time == 0){
	obj_item_screen.y=233;
}

if (state_time > 8 && input.start) {
	room_goto(rm_select);
	state_switch(st_game_select);
}

if (!audio_is_playing(snd_music_foreboding)) {
	room_goto(rm_title);
	state_switch(st_game_title);
}

move_speed = 0.38;
if (state_time >= 20 && obj_item_screen.y > 0) {
	obj_item_screen.y -= move_speed;
}
if (state_time >= 900 && obj_item_screen.y > 232-obj_item_screen.sprite_height){
	obj_item_screen.y -= move_speed;
}