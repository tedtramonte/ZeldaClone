if (input.up) state = "UP";
if (input.down) state = "DOWN";
if (input.left) state = "LEFT";
if (input.right) state = "RIGHT";
if (!input.up && !input.down && !input.left && !input.right) state = "IDLE";
if (input.button_a) state = "ATTACK_SWORD_START";